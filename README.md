# Currency Microservice

### Implementation Summary

#### Considerations
The external API resource contains some currency codes which are not recognised under ISO 4217. To confirm to Postel’s Law a.k.a the Robustness Principle and being liberal in what you accept, I would have suggested we provide a response even for unofficial codes rather than simply a 404 not found, however the requirements were clear that the query parameter must be an ISO 4217 alphabetic currency code so I have removed unofficial currency codes from consideration. 

Where the query parameter input cannot be accepted (in the case of incorrectly formatted currency codes for instance), I have implemented pre-condition checks on the controller layer with a regex pattern validator, such that if the query parameter is not in ISO 4217 format, a code 400 response is returned, additionally including the pre-condition check which has failed, to ‘fail fast’ and be explicit to the client as to why their input could not be accepted. 

#### Performance 
Due to the size of the dataset and the nature of the data (currency codes/borders which rarely change), it makes sense to cache the results of all valid currency codes that might be passed as query parameter to this API. Doing so will reduce API response times from over 500ms to about 10ms. Caching the data will also remove dependency on the external API’s availability, so even if the external API is unavailable due to a loss of service, our API will continue to perform unaffected. 

Responses for all valid currency codes passed as query parameter have been pre-processed and cached into a HashMap for fast look up via key (currency code) reference so there is no additional processing required at runtime which would have otherwise impacted performance.

#### Configuration
A midnight background process has been configured to refresh the cached data, every day at midnight such that if there is a change in a country’s currency code/border then the data we are caching will be refreshed daily; this is achieved via a cron variable in application.properties file which is currently set to midnight; DevOps can easily amend this variable to a different time without the need for a code change in the application. Additionally, restarting the application will also refresh the cached data. 
If there was a requirement for our API to be synchronised to changes in the external API’s data then a possible solution might be to create a separate API to schedule a date/time planned for a currency/border change and refresh the data at that time.

The URL we are using to access the external API has been added into application properties such that, if the URL changes for some reason, DevOps can also simply amend this external API URL path.

#### Response
While the requirements were clear about the structure of the JSON response, I would have recommended including the currency code query parameter in the response to make it clear to the client that we are both on the same page as to what the response is referring to. 

#### Response Codes

Implemented 200, 400, 404 and 503 http status codes and exception handling. I made a decision not to stop the application from starting if the external API is initially unavailable as this could negatively impact clients relying our service, instead I chose to return a 503 service unavailable status code to allow the client to more gracefully respond to this.

#### Testing

4 Tests have been written for QA, confirming logic is performing as expected, the correct response codes are returned, response times outperform external API and mappers are working correctly to filter out null/invalid/duplicate currency codes.

#### Build

Install JDK 1.8

Clone repo from GitLab repository.

Run maven package command to compile JAR

Ensure no other process is running on port 80.

Navigate to directory containing outputted JAR, run command: java -jar currency-1.0.0.jar to start application. 

