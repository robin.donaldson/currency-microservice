package com.emishealth.data;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
public class CountryMockDataConfig {

    private String countryCode;

    private String currencyCode;

    private Set<String> borders;

}
