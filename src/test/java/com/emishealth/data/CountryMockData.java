package com.emishealth.data;

import com.emishealth.model.CountryEntity;
import com.emishealth.model.CurrencyEntity;

import java.util.*;

public class CountryMockData {

    public List<CountryEntity> getCountryListMockData(CountryMockDataConfig countryMockDataConfig) {
        List<CountryEntity> countryEntityList = new ArrayList<>();
        CountryEntity countryEntity = getCountryMockData(countryMockDataConfig);
        countryEntityList.add(countryEntity);
        return countryEntityList;
    }

    private CountryEntity getCountryMockData(CountryMockDataConfig countryMockDataConfig) {

        CountryEntity countryEntity = new CountryEntity();
        countryEntity.setAlpha3Code(countryMockDataConfig.getCountryCode());

        Set<CurrencyEntity> currencies = new HashSet<>();
        CurrencyEntity currencyEntity = new CurrencyEntity();
        currencyEntity.setCode(countryMockDataConfig.getCurrencyCode());
        currencies.add(currencyEntity);
        countryEntity.setCurrencies(currencies);

        countryEntity.setBorders(countryMockDataConfig.getBorders());

        return countryEntity;
    }

}
