package com.emishealth.data;

import com.emishealth.model.CountryEntity;
import com.emishealth.model.CurrencyEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class BorderInfoExternalApi {

    @Autowired
    private TestRestTemplate restTemplate;

    @Value("${allCountriesExternalApi}")
    private String allCountriesExternalApi;

    @Value("${countryExternalApi}")
    private String countryExternalApi;

    @Value("${currencyExternalApi}")
    private String currencyExternalApi;

    private List<CountryEntity> getAllCountries() {
        ResponseEntity<CountryEntity[]> responseEntity = restTemplate.getForEntity(allCountriesExternalApi, CountryEntity[].class);
        return Arrays.asList(Objects.requireNonNull(responseEntity.getBody()));
    }

    public List<String> getAllCurrencyCodes() {
        return getAllCountries()
                .stream()
                .map(CountryEntity::getCurrencies)
                .flatMap(currency -> currency.stream().map(CurrencyEntity::getCode))
                .filter(currency -> Objects.nonNull(currency) && currency.matches("^[a-zA-Z]{2,3}$"))
                .distinct()
                .collect(Collectors.toList());
    }

    private CountryEntity getCountryByCode(String countryCode) {
        URI uri = UriComponentsBuilder.fromHttpUrl(countryExternalApi + countryCode).build().encode().toUri();
        ResponseEntity<CountryEntity> responseEntity = restTemplate.getForEntity(uri, CountryEntity.class);
        return Objects.requireNonNull(responseEntity.getBody());
    }

    public List<String> countryCodesUsingCurrency(String currencyCode) {
        URI uri = UriComponentsBuilder.fromHttpUrl(currencyExternalApi + currencyCode).build().encode().toUri();
        ResponseEntity<CountryEntity[]> responseEntity = restTemplate.getForEntity(uri, CountryEntity[].class);
        return Arrays.stream(Objects.requireNonNull(responseEntity.getBody()))
                .map(CountryEntity::getAlpha3Code)
                .sorted()
                .collect(Collectors.toList());
    }

    public List<String> countryCodesSharingBordersButNotCurrency(String currencyCode) {

        List<String> countryCodesUsingCurrency = countryCodesUsingCurrency(currencyCode);
        Set<String> countriesSharingBordersButNotCurrency = new HashSet<>();

        for (String countryCode: countryCodesUsingCurrency) {
            CountryEntity countryEntity = getCountryByCode(countryCode);
            Set<String> borders = countryEntity.getBorders();
            borders.removeIf(countryCodesUsingCurrency::contains);
            countriesSharingBordersButNotCurrency.addAll(borders);
        }

        return countriesSharingBordersButNotCurrency.stream()
                .sorted()
                .collect(Collectors.toList());

    }

}
