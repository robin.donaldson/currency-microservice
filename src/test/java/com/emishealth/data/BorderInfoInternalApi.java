package com.emishealth.data;

import io.swagger.model.CountryDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

@Component
public class BorderInfoInternalApi {

    @Autowired
    private TestRestTemplate restTemplate;

    @Value("${localHost}")
    private String localHost;

    @Value("${borderInfoInternalApi}")
    private String borderInfoInternalApi;

    public ResponseEntity<CountryDTO> responseEntityByCurrencyCode(int port, String currencyCode) {
        URI uri = UriComponentsBuilder.fromHttpUrl(localHost + port + borderInfoInternalApi).queryParam("currency", currencyCode).build().encode().toUri();
        return restTemplate.getForEntity(uri, CountryDTO.class);
    }

    public List<String> countryCodesUsingCurrencyCode(int port, String currencyCode) {
        CountryDTO countryDTO = responseEntityByCurrencyCode(port, currencyCode).getBody();
        if (countryDTO != null) {
            return countryDTO.getSupportedCountryCodes();
        } else {
            return new ArrayList<>();
        }
    }

    public List<String> countryCodesSharingBordersButNotCurrencyCode(int port, String currencyCode) {
        CountryDTO countryDTO = responseEntityByCurrencyCode(port, currencyCode).getBody();
        if (countryDTO != null) {
            return countryDTO.getBorderCountryCodes();
        } else {
            return new ArrayList<>();
        }
    }

}
