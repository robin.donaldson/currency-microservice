package com.emishealth.service;

import com.emishealth.data.CountryMockData;
import com.emishealth.data.CountryMockDataConfig;
import com.emishealth.model.CountryEntity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.springframework.test.util.AssertionErrors.assertEquals;

@RunWith(SpringRunner.class)
public class SharedCurrencyServiceTest {

    @TestConfiguration
    static class SharedCurrencyServiceTestConfiguration {
        @Bean
        public SharedCurrencyService sharedCurrencyService() {
            return new SharedCurrencyService();
        }
    }

    @Autowired
    SharedCurrencyService sharedCurrencyService;

    private CountryMockData countryMockData = new CountryMockData();

    @Test
    public void sharedCurrencyServiceTest() {
        List<CountryEntity> countryEntityList = countryMockData.getCountryListMockData(new CountryMockDataConfig("FRA", "EUR", null));
        countryEntityList.addAll(countryMockData.getCountryListMockData(new CountryMockDataConfig("DEU", "EUR", null)));
        assertEquals("France + Germany share same currency", sharedCurrencyService.getCountriesSharingCurrencyCode(countryEntityList).get("EUR").size(), 2);
    }

}
