package com.emishealth.controller;

import com.emishealth.data.BorderInfoExternalApi;
import com.emishealth.data.BorderInfoInternalApi;
import io.swagger.model.CountryDTO;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestPropertySource(locations="classpath:test.properties")
class BorderInfoControllerTest {

    @LocalServerPort
    private int port;

    @Autowired
    BorderInfoExternalApi borderInfoExternalApi;

    @Autowired
    BorderInfoInternalApi borderInfoInternalApi;

    private List<String> allCurrencyCodes;

    @BeforeAll
    void beforeAll() {
        allCurrencyCodes = borderInfoExternalApi.getAllCurrencyCodes();
    }

    @Test
    void status200CurrencyCodeValidTest1() {
        ResponseEntity<CountryDTO> responseEntity = borderInfoInternalApi.responseEntityByCurrencyCode(port, "GBP");
        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(200);
    }

    @Test
    void status200CurrencyCodeValidTest2() {
        ResponseEntity<CountryDTO> responseEntity = borderInfoInternalApi.responseEntityByCurrencyCode(port, "EUR");
        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(200);
    }

    @Test
    void status200CurrencyCodeValidTest3() {
        ResponseEntity<CountryDTO> responseEntity = borderInfoInternalApi.responseEntityByCurrencyCode(port, "USD");
        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(200);
    }

    @Test
    void status200CurrencyCodeCaseInsensitiveTest() {
        ResponseEntity<CountryDTO> responseEntity = borderInfoInternalApi.responseEntityByCurrencyCode(port, "gbp");
        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(200);
    }

    @Test
    void status400CurrencyCodeNullTest() {
        ResponseEntity<CountryDTO> responseEntity = borderInfoInternalApi.responseEntityByCurrencyCode(port, null);
        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(400);
    }

    @Test
    void status400CurrencyCodeEmptyTest() {
        ResponseEntity<CountryDTO> responseEntity = borderInfoInternalApi.responseEntityByCurrencyCode(port, "");
        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(400);
    }

    @Test
    void status400CurrencyCodeTooShortTest() {
        ResponseEntity<CountryDTO> responseEntity = borderInfoInternalApi.responseEntityByCurrencyCode(port, "G");
        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(400);
    }

    @Test
    void status400CurrencyCodeTooLongTest() {
        ResponseEntity<CountryDTO> responseEntity = borderInfoInternalApi.responseEntityByCurrencyCode(port, "GGGG");
        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(400);
    }

    @Test
    void status400CurrencyCodeInvalidCharacterTest1() {
        ResponseEntity<CountryDTO> responseEntity = borderInfoInternalApi.responseEntityByCurrencyCode(port, "G/A");
        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(400);
    }

    @Test
    void status400CurrencyCodeInvalidCharacterTest2() {
        ResponseEntity<CountryDTO> responseEntity = borderInfoInternalApi.responseEntityByCurrencyCode(port, "G.A");
        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(400);
    }

    @Test
    void status404CurrencyCodeNotFoundTest() {
        ResponseEntity<CountryDTO> responseEntity = borderInfoInternalApi.responseEntityByCurrencyCode(port, "AAA");
        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(404);
    }

    @Test
    void validateCountriesSharingCurrenciesAgainstExternalApi() {
        for (int i = 0; i < 2; i++) { // increase for loop to test more currency codes
            assertThat(borderInfoInternalApi.countryCodesUsingCurrencyCode(port, allCurrencyCodes.get(i))).isEqualTo(borderInfoExternalApi.countryCodesUsingCurrency(allCurrencyCodes.get(i)));
        }
    }

    @Test
    void validateCountriesSharingBordersButNotCurrenciesAgainstExternalApi() {
        for (int i = 0; i < 2; i++) { // increase for loop to test more currency codes
            List<String> internalApiBordersCheck = borderInfoInternalApi.countryCodesSharingBordersButNotCurrencyCode(port, allCurrencyCodes.get(i));
            List<String> externalApiBordersCheck = borderInfoExternalApi.countryCodesSharingBordersButNotCurrency(allCurrencyCodes.get(i));
            assertThat(internalApiBordersCheck).isEqualTo(externalApiBordersCheck);
        }
    }

    @Test
    void internalVsExternalApiResponseTimeTest() {

        long time = System.currentTimeMillis();
        borderInfoInternalApi.countryCodesUsingCurrencyCode(port, "AUD");
        long internalResponseTimeMs = (System.currentTimeMillis() - time);

        time = System.currentTimeMillis();
        borderInfoExternalApi.countryCodesUsingCurrency("AUD");
        long externalResponseTimeMs = (System.currentTimeMillis() - time);

        assertThat(internalResponseTimeMs).isLessThan(externalResponseTimeMs);
    }

}
