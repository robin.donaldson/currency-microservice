package com.emishealth.mapping;

import com.emishealth.data.CountryMockDataConfig;
import com.emishealth.model.CountryEntity;
import com.emishealth.data.CountryMockData;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.springframework.test.util.AssertionErrors.assertEquals;

@RunWith(SpringRunner.class)
public class CountryMapperTest {

    @TestConfiguration
    static class CountryMapperTestConfiguration {
        @Bean
        public CountryMapper countryMapper() {
            return new CountryMapper();
        }
    }

    @Autowired
    CountryMapper countryMapper;

    private CountryMockData countryMockData = new CountryMockData();

    @Test
    public void mapValidCurrencyCodeTest() {
        List<CountryEntity> countryEntityList = countryMockData.getCountryListMockData(new CountryMockDataConfig("GBR", "GBP", null));
        List<CountryEntity> mappedCountries = countryMapper.mapCountries(countryEntityList);
        assertEquals("CountryMapper maps valid currency codes", mappedCountries.get(0).getCurrencies().size(), countryEntityList.get(0).getCurrencies().size());
    }

    @Test
    public void mapCurrencyCodeNullTest() {
        List<CountryEntity> countryEntityList = countryMockData.getCountryListMockData(new CountryMockDataConfig("GBR", null, null));
        List<CountryEntity> mappedCountries = countryMapper.mapCountries(countryEntityList);
        assertEquals("CountryMapper filters out null currency codes", mappedCountries.get(0).getCurrencies().size(), 0);
    }

    @Test
    public void mapCurrencyCodeEmptyTest() {
        List<CountryEntity> countryEntityList = countryMockData.getCountryListMockData(new CountryMockDataConfig("GBR", "", null));
        List<CountryEntity> mappedCountries = countryMapper.mapCountries(countryEntityList);
        assertEquals("CountryMapper filters out empty currency codes", mappedCountries.get(0).getCurrencies().size(), 0);
    }

    @Test
    public void mapCurrencyCodeTooShortTest() {
        List<CountryEntity> countryEntityList = countryMockData.getCountryListMockData(new CountryMockDataConfig("GBR", "G", null));
        List<CountryEntity> mappedCountries = countryMapper.mapCountries(countryEntityList);
        assertEquals("CountryMapper filters out too short currency codes", mappedCountries.get(0).getCurrencies().size(), 0);
    }

    @Test
    public void mapCurrencyCodeTooLongTest() {
        List<CountryEntity> countryEntityList = countryMockData.getCountryListMockData(new CountryMockDataConfig("GBR", "GGGG", null));
        List<CountryEntity> mappedCountries = countryMapper.mapCountries(countryEntityList);
        assertEquals("CountryMapper filters out too long currency codes", mappedCountries.get(0).getCurrencies().size(), 0);
    }

    @Test
    public void mapCurrencyCodeCaseInsensitiveTest() {
        List<CountryEntity> countryEntityList = countryMockData.getCountryListMockData(new CountryMockDataConfig("GBR", "gbp", null));
        List<CountryEntity> mappedCountries = countryMapper.mapCountries(countryEntityList);
        assertEquals("CountryMapper filters out too long currency codes", mappedCountries.get(0).getCurrencies().size(), 1);
    }

    @Test
    public void mapCurrencyCodeInvalidCharacterTest() {
        List<CountryEntity> countryEntityList = countryMockData.getCountryListMockData(new CountryMockDataConfig("GBR", "G/A", null));
        List<CountryEntity> mappedCountries = countryMapper.mapCountries(countryEntityList);
        assertEquals("CountryMapper filters out invalid character currency codes", mappedCountries.get(0).getCurrencies().size(), countryEntityList.get(0).getCurrencies().size());
    }

}
