package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * CountryDTO
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2021-01-10T17:43:13.345Z")

public class CountryDTO   {
  @JsonProperty("supportedCountryCodes")
  private List<String> supportedCountryCodes = null;

  @JsonProperty("borderCountryCodes")
  private List<String> borderCountryCodes = null;

  public CountryDTO supportedCountryCodes(List<String> supportedCountryCodes) {
    this.supportedCountryCodes = supportedCountryCodes;
    return this;
  }

  public CountryDTO addSupportedCountryCodesItem(String supportedCountryCodesItem) {
    if (this.supportedCountryCodes == null) {
      this.supportedCountryCodes = new ArrayList<String>();
    }
    this.supportedCountryCodes.add(supportedCountryCodesItem);
    return this;
  }

   /**
   * Get supportedCountryCodes
   * @return supportedCountryCodes
  **/
  @ApiModelProperty(value = "")


  public List<String> getSupportedCountryCodes() {
    return supportedCountryCodes;
  }

  public void setSupportedCountryCodes(List<String> supportedCountryCodes) {
    this.supportedCountryCodes = supportedCountryCodes;
  }

  public CountryDTO borderCountryCodes(List<String> borderCountryCodes) {
    this.borderCountryCodes = borderCountryCodes;
    return this;
  }

  public CountryDTO addBorderCountryCodesItem(String borderCountryCodesItem) {
    if (this.borderCountryCodes == null) {
      this.borderCountryCodes = new ArrayList<String>();
    }
    this.borderCountryCodes.add(borderCountryCodesItem);
    return this;
  }

   /**
   * Get borderCountryCodes
   * @return borderCountryCodes
  **/
  @ApiModelProperty(value = "")


  public List<String> getBorderCountryCodes() {
    return borderCountryCodes;
  }

  public void setBorderCountryCodes(List<String> borderCountryCodes) {
    this.borderCountryCodes = borderCountryCodes;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CountryDTO countryDTO = (CountryDTO) o;
    return Objects.equals(this.supportedCountryCodes, countryDTO.supportedCountryCodes) &&
        Objects.equals(this.borderCountryCodes, countryDTO.borderCountryCodes);
  }

  @Override
  public int hashCode() {
    return Objects.hash(supportedCountryCodes, borderCountryCodes);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CountryDTO {\n");
    
    sb.append("    supportedCountryCodes: ").append(toIndentedString(supportedCountryCodes)).append("\n");
    sb.append("    borderCountryCodes: ").append(toIndentedString(borderCountryCodes)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

