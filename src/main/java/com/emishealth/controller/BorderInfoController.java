package com.emishealth.controller;

import com.emishealth.service.BorderInfoService;
import io.swagger.api.BorderInfoApi;
import io.swagger.model.CountryDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@Validated
public class BorderInfoController implements BorderInfoApi {

    private final BorderInfoService borderInfoService;

    public BorderInfoController(BorderInfoService borderInfoService) {
        this.borderInfoService = borderInfoService;
    }

    @Override
    public ResponseEntity<CountryDTO> borderInfoGet(@RequestParam(value = "currency", required = true) String currency) {
        CountryDTO countryDTO = borderInfoService.borderInfoGet(currency);
        return new ResponseEntity<>(countryDTO, HttpStatus.OK);
    }

}
