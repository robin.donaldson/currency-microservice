package com.emishealth.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CurrencyEntity {

    private String code;

    private String name;

    private String symbol;

}
