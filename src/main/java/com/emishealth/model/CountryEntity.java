package com.emishealth.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@JsonIgnoreProperties(ignoreUnknown=true)
@Getter
@Setter
public class CountryEntity {

    private String alpha3Code;

    private Set<String> borders;

    private Set<CurrencyEntity> currencies;

}
