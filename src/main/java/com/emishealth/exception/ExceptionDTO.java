package com.emishealth.exception;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.util.List;

@Getter
@Setter
class ExceptionDTO {

    private Integer status;
    private String message;
    private List<String> validationErrors;

    ExceptionDTO(HttpStatus httpStatus, List<String> validationErrors) {
        this.status = httpStatus.value();
        this.message = httpStatus.getReasonPhrase();
        this.validationErrors = validationErrors;
    }

}


