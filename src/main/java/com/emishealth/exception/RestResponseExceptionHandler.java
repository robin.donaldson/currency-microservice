package com.emishealth.exception;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.http.HttpStatus.*;

@ControllerAdvice
public class RestResponseExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<ExceptionDTO> handleNotFoundException(Exception ex){
        ex.printStackTrace();
        ExceptionDTO exceptionDTO = new ExceptionDTO(NOT_FOUND, null);
        return new ResponseEntity<>(exceptionDTO, NOT_FOUND);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<ExceptionDTO> handleConstraintViolationExceptions(ConstraintViolationException ex) {
        ex.printStackTrace();
        List<String> errors = ex.getConstraintViolations()
                .stream()
                .map(ConstraintViolation::getMessage)
                .collect(Collectors.toList());
        ExceptionDTO exceptionDTO = new ExceptionDTO(BAD_REQUEST, errors);
        return new ResponseEntity<>(exceptionDTO, BAD_REQUEST);
    }

    @ExceptionHandler(ServiceUnavailableException.class)
    public ResponseEntity<ExceptionDTO> handleUnavailableException(Exception ex) {
        ex.printStackTrace();
        ExceptionDTO exceptionDTO = new ExceptionDTO(SERVICE_UNAVAILABLE,null);
        return new ResponseEntity<>(exceptionDTO, SERVICE_UNAVAILABLE);
    }

}
