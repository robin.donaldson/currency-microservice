package com.emishealth.mapping;

import com.emishealth.model.CountryEntity;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class CountryMapper {

    // remove null/invalid currency codes from CountryEntities List and additionally capitalise all currency codes to avoid case sensitive issues later
    public List<CountryEntity> mapCountries(List<CountryEntity> countryEntities) {
        return countryEntities.stream()
                .peek(countryEntity -> countryEntity.setCurrencies(
                        countryEntity.getCurrencies()
                                .stream()
                                .filter(currency -> Objects.nonNull(currency.getCode()) && currency.getCode().matches("^[a-zA-Z]{2,3}$"))
                                .peek(currencyEntity -> currencyEntity.setCode(currencyEntity.getCode().toUpperCase()))
                                .collect(Collectors.toSet())))
                .collect(Collectors.toList());
    }

}
