package com.emishealth.service;

import com.emishealth.model.CountryEntity;
import com.emishealth.model.CurrencyEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SharedCurrencyService {

    // returns Map - Key: currency code, Value: List of country codes using currency
    Map<String, List<String>> getCountriesSharingCurrencyCode(List<CountryEntity> countryEntityList) {

        Map<String, List<String>> currencyCodeUsedInCountriesMap = new HashMap<>();

        for (CountryEntity countryEntity : countryEntityList) {
            for (CurrencyEntity currencyEntity : countryEntity.getCurrencies()) {
                currencyCodeUsedInCountriesMap.computeIfAbsent(currencyEntity.getCode(), k -> new ArrayList<>());
                currencyCodeUsedInCountriesMap.get(currencyEntity.getCode()).add(countryEntity.getAlpha3Code());
            }
        }

        return currencyCodeUsedInCountriesMap;

    }

}
