package com.emishealth.service;

import com.emishealth.model.CountryEntity;
import com.emishealth.model.CurrencyEntity;
import io.swagger.model.CountryDTO;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class BorderInfoAotService {

    private final SharedBordersService sharedBordersService;
    private final SharedCurrencyService sharedCurrencyService;

    public BorderInfoAotService(SharedBordersService sharedBordersService, SharedCurrencyService sharedCurrencyService) {
        this.sharedBordersService = sharedBordersService;
        this.sharedCurrencyService = sharedCurrencyService;
    }

    Map<String, CountryDTO> getCountriesDtoResultMap(List<CountryEntity> countryEntityList) {

        Set<String> currencyCodeSet = getUniqueCurrencyCodes(countryEntityList);
        Map<String, List<String>> currencyUsedInCountriesMap = sharedCurrencyService.getCountriesSharingCurrencyCode(countryEntityList);
        Map<String, List<String>> countryCodesSharingBordersButNotCurrencyMap = sharedBordersService.getCountriesSharingBordersButNotCurrencyCode(currencyUsedInCountriesMap, countryEntityList);

        Map<String, CountryDTO> countriesDtoResultMap = new HashMap<>();

        for (String currency : currencyCodeSet) {
            CountryDTO countryDTO = new CountryDTO();
            countryDTO.setSupportedCountryCodes(sortDistinct(currencyUsedInCountriesMap.get(currency)));
            countryDTO.setBorderCountryCodes(sortDistinct(countryCodesSharingBordersButNotCurrencyMap.get(currency)));
            countriesDtoResultMap.put(currency, countryDTO);
        }

        return countriesDtoResultMap;

    }

    private Set<String> getUniqueCurrencyCodes(List<CountryEntity> countryEntityList) {
        return countryEntityList
                .stream()
                .map(CountryEntity::getCurrencies)
                .flatMap(currency -> currency.stream().map(CurrencyEntity::getCode))
                .collect(Collectors.toSet());
    }

    private List<String> sortDistinct(List<String> strList) {
        return strList.stream()
                .distinct()
                .sorted()
                .collect(Collectors.toList());
    }

}
