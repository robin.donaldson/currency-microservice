package com.emishealth.service;

import com.emishealth.model.CountryEntity;
import com.emishealth.model.CurrencyEntity;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class SharedBordersService {

    // returns Map - Key: currency code, Value: List of country codes sharing borders but not currency
    Map<String, List<String>> getCountriesSharingBordersButNotCurrencyCode(Map<String, List<String>> currencyUsedInCountriesMap, List<CountryEntity> countryEntityList) {

        Map<String, CountryEntity> countryCodeMap = countryEntityList.stream().collect(Collectors.toMap(country -> country.getAlpha3Code().toUpperCase(), country -> country));

        Map<String, List<String>> countryCodeSharingBordersButNotCurrencyMap = new HashMap<>();
        for (Map.Entry<String, List<String>> entry : currencyUsedInCountriesMap.entrySet()) {

            String currency = entry.getKey();
            List<String> countriesUsingCurrency = entry.getValue();
            List<String> countriesSharingBordersButNotCurrency = new ArrayList<>();

            for (String countryCode: countriesUsingCurrency) {
                Set<String> borders = new HashSet<>(countryCodeMap.get(countryCode).getBorders());
                borders.removeIf(countriesUsingCurrency::contains);
                countriesSharingBordersButNotCurrency.addAll(borders);
            }

            countryCodeSharingBordersButNotCurrencyMap.put(currency, countriesSharingBordersButNotCurrency);

        }

        return countryCodeSharingBordersButNotCurrencyMap;

    }

}
