package com.emishealth.service;

import com.emishealth.exception.NotFoundException;
import com.emishealth.exception.ServiceUnavailableException;
import io.swagger.model.CountryDTO;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class BorderInfoService {

    private final BorderInfoManagementService borderInfoManagementService;

    public BorderInfoService(BorderInfoManagementService borderInfoManagementService) {
        this.borderInfoManagementService = borderInfoManagementService;
    }

    public CountryDTO borderInfoGet(String currency) {

        Map<String, CountryDTO> countryDTOMap = borderInfoManagementService.countryDtoResultMap;

        if (countryDTOMap.size() == 0) {
            throw new ServiceUnavailableException();
        }

        currency = currency.toUpperCase();

        CountryDTO countryDTO = countryDTOMap.get(currency);
        if (countryDTO == null) {
            throw new NotFoundException();
        }

        return countryDTO;
    }
}
