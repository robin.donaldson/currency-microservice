package com.emishealth.service;

import com.emishealth.mapping.CountryMapper;
import com.emishealth.model.CountryEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.*;

@Service
public class CountryQueryService {

    @Value("${allCountriesExternalApi}")
    private String allCountriesExternalApi;

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private final CountryMapper countryMapper;
    private final RestTemplate restTemplate;

    public CountryQueryService(CountryMapper countryMapper, RestTemplateBuilder restTemplateBuilder) {
        this.countryMapper = countryMapper;
        this.restTemplate = restTemplateBuilder.build();
    }

    Optional<List<CountryEntity>> loadCountriesViaExternalAPI() {
        try {
            ResponseEntity<CountryEntity[]> responseEntity = restTemplate.getForEntity(allCountriesExternalApi, CountryEntity[].class);
            if (responseEntity.getBody() != null) {
                return Optional.of(countryMapper.mapCountries(Arrays.asList(responseEntity.getBody())));
            } else {
                logger.error("Error fetching country info from external API URL: " + allCountriesExternalApi + " error: response body is null");
                return Optional.empty();
            }
        } catch (Exception e) {
            logger.error("Error fetching country info from external API URL: " + allCountriesExternalApi + " error: " + e.getMessage());
            return Optional.empty();
        }
    }

}
