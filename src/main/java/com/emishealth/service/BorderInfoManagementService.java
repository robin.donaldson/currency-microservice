package com.emishealth.service;

import com.emishealth.model.CountryEntity;
import io.swagger.model.CountryDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

/*
    Due to the size of the country dataset (250 records) and the nature of the data (currency codes/borders which rarely change),
    it makes sense to cache the results of all valid currency codes that might be passed as query parameter to this API.
    Doing so will reduce API response times from 500ms to 10ms.
    Caching the data will also remove dependency on the external API’s availability, so even if the external API is unavailable due to a loss of service,
    our API will still continue to perform unaffected.
 */

@Service
public class BorderInfoManagementService {

    private final CountryQueryService countryQueryService;
    private final BorderInfoAotService borderInfoAotService;
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    Map<String, CountryDTO> countryDtoResultMap = new ConcurrentHashMap<>();

    public BorderInfoManagementService(BorderInfoAotService borderInfoAotService, CountryQueryService countryQueryService) {
        this.borderInfoAotService = borderInfoAotService;
        this.countryQueryService = countryQueryService;
    }

    @PostConstruct
    public void initBorderInfo() {
        fetchAndPreProcessBorderInfo();
    }

    @Scheduled(cron="${midnight}")
    public void refreshBorderInfo() {
        fetchAndPreProcessBorderInfo();
    }

    private void fetchAndPreProcessBorderInfo() {
        Optional<List<CountryEntity>> optionalCountries = countryQueryService.loadCountriesViaExternalAPI();
        if (optionalCountries.isPresent()) {
            List<CountryEntity> countryEntityList = optionalCountries.get();
            countryDtoResultMap = borderInfoAotService.getCountriesDtoResultMap(countryEntityList);
            logger.info("Successfully processed country information obtained via external API");
        }
    }

}
